Input = require 'Input'

function love.load()
  -- love.window.setFullscreen(true)
  input = Input()

  allkeys = {
  "a",
  "b",
  "c",
  "d",
  "e",
  "f",
  "g",
  "h",
  "i",
  "j",
  "k",
  "l",
  "m",
  "n",
  "o",
  "p",
  "q",
  "r",
  "s",
  "t",
  "u",
  "v",
  "w",
  "x",
  "y",
  "z",
  "1",
  "2",
  "3",
  "4",
  "5",
  "6",
  "7",
  "8",
  "9",
  "0",
  "return",
  "escape",
  "backspace",
  "tab",
  "space",
  "-",
  "=",
  "[",
  "]",
  "\\",
  ";",
  "'",
  "`",
  ",",
  ".",
  "/",
  "capslock",
  "f1",
  "f2",
  "f3",
  "f4",
  "f5",
  "f6",
  "f7",
  "f8",
  "f9",
  "f10",
  "f11",
  "f12",
  "f13",
  "f14",
  "f15",
  "f16",
  "f17",
  "f18",
  "f19",
  "f20",
  "f21",
  "f22",
  "f23",
  "f24",
  "lctrl",
  "lshift",
  "lalt",
  "lgui",
  "rctrl",
  "rshift",
  "ralt",
  "rgui",
  "printscreen",
  "scrolllock",
  "pause",
  "insert",
  "home",
  "numlock",
  "pageup",
  "delete",
  "end",
  "pagedown",
  "right",
  "left",
  "down",
  "up",
  "application",
  "execute",
  "help",
  "menu",
  "select",
  "stop",
  "again",
  "undo",
  "cut",
  "copy",
  "paste",
  "find",
  "kp/",
  "kp*",
  "kp-",
  "kp+",
  "kp=",
  "kpenter",
  "kp1",
  "kp2",
  "kp3",
  "kp4",
  "kp5",
  "kp6",
  "kp7",
  "kp8",
  "kp9",
  "kp0",
  "kp.",
  "mute",
  "volumeup",
  "volumedown",
  "audionext",
  "audioprev",
  "audiostop",
  "audioplay",
  "audiomute",
  "mediaselect",
  "www",
  "mail",
  "calculator",
  "computer",
  "sysreq",
  "cancel",
  "clear",
  "prior",
  "return2",
  "separator",
  "out",
  "oper",
  "clearagain",
  "unknown"}

  x = 2
  y = 2
  onRoomWarper = false

  BLANKTILE = 0
  WALLTILE = 1
  PREVROOMWARPERTILE = 2
  NEXTROOMWARPERTILE = 3
  COINTILE = 4

  MAXSEED = 100
  MINSEED = 0

  math.randomseed(os.time())
  --mapseed = math.random(0, 100)
  mapseed = 1
  -- maxUnits = 15
  maxXUnits = 15
  maxYUnits = 15

  elapsedTime = 0
  mazes = {}
  -- mapgrid = buildDumbMap(mapseed)
  mapgrid = buildLessDumbMap(mapseed)

  viewwidth = love.graphics.getWidth( )
  viewheight = love.graphics.getHeight( )

  unitWidth = viewwidth / maxXUnits;
  unitHeight = viewheight / maxYUnits;

  coinget = love.audio.newSource("coinget.wav", "static")

  soundfiles = love.filesystem.getDirectoryItems( "sounds" )
  numsoundfiles = #soundfiles
  print('num sound files '..numsoundfiles)
  soundsources = {};
  --
  -- for c in soundkeys:gmatch"." do
  --     i = string.byte(c) - 96
  --
  --     soundsources[i] = love.audio.newSource("sounds/"..soundfiles[i], "static")
  -- end

  for k, v in pairs(allkeys) do
    keyzeroed = k - 1
    print('key '..keyzeroed..', value "'..v..'"')
    print('keyzeroed mod numsoundfiles + 1 = '..(keyzeroed%numsoundfiles) + 1)

    soundsources[v] = love.audio.newSource("sounds/"..soundfiles[(keyzeroed%numsoundfiles) + 1], "static")
  end


  numjoysticks = #input.joysticks

  input:bind('up', 'up')
  input:bind('down', 'down')
  input:bind('left', 'left')
  input:bind('right', 'right')
  input:bind('escape', 'escape')
  input:bind('return', 'return')

  if numjoysticks > 0 then
    input:bind('dpup', 'up')
    input:bind('dpdown', 'down')
    input:bind('dpleft', 'left')
    input:bind('dpright', 'right')


    input:bind('fup', 'up')
    input:bind('fdown', 'down')
    input:bind('fleft', 'left')
    input:bind('fright', 'right')
    input:bind('leftx', 'leftx')
    input:bind('lefty', 'lefty')
    input:bind('rightx', 'rightx')
    input:bind('righty', 'righty')

    input:bind('start', 'start')
    input:bind('back', 'back')
  end

  print('num joypads '..numjoysticks)

  print(viewwidth)
  print(viewheight)
  print(unitWidth)
  print(unitHeight)

end

function love.update(dt)
  --    print(elapsedTime .. " - " .. dt)
  elapsedTime = elapsedTime + dt

  if elapsedTime > 0.1 then
    elapsedTime = 0
    prevx = x
    prevy = y

    -- if love.keyboard.isDown("right") then
    --     x = x + 1
    -- elseif love.keyboard.isDown("left") then
    --     x = x - 1
    -- end

    -- if numjoysticks > 0 and input:sequence('back', 1, 'start') then
    -- if input:sequence('escape', 1, 'return') then
    -- --if input:sequence('p', 1, 'a') then
    --   love.event.quit()
    -- end

    curleftx = 0
    curlefty = 0
    currightx = 0
    currighty = 0

    if numjoysticks > 0 then
      curleftx = getAxisValue(input:down('leftx'))
      curlefty = getAxisValue(input:down('lefty'))
      currightx = getAxisValue(input:down('rightx'))
      currighty = getAxisValue(input:down('righty'))
    end

    if input:down('right') or currightx > 0 or curleftx > 0 then
      x = x + 1
    elseif input:down('left') or currightx < 0 or curleftx < 0 then
      x = x - 1
    end


    for k, v in pairs(allkeys) do
      if love.keyboard.isDown(v) then
          soundsources[v]:play()
        end
    end

    if collisionWithMap() then
      x = prevx
    end

    if input:down('down') or currighty > 0 or curlefty > 0 then
      y = y + 1
    elseif input:down('up') or currighty < 0 or curlefty < 0 then
      y = y - 1
    end

    if collisionWithMap() then
      y = prevy
    end

    if collisionWithCoinTile() then
      coinget:stop()
      coinget:play()
      mapgrid[x][y] = BLANKTILE
    end

    if not onRoomWarper then
      if collisionWithNextRoom() then
        saveMap()
        mapseed = mapseed + 1
        if mapseed > MAXSEED then
          mapseed = MINSEED
        end
        mapgrid = buildLessDumbMap(mapseed)

        onRoomWarper = true

        x = 1
        y = prevYValue()
      elseif collisionWithPrevRoom() then
        saveMap()
        mapseed = mapseed - 1
        if mapseed < MINSEED then
          mapseed = MAXSEED
        end
        mapgrid = buildLessDumbMap(mapseed)

        onRoomWarper = true

        x = maxXUnits
        y = nextYValue()
      end
    end

    if currentlyOnWalkableArea() then
      onRoomWarper = false
    end

  end
end

function love.draw()
  drawlevel()
  drawactors()
end

function drawactors()
  love.graphics.setColor(224, 214, 68)
  -- love.graphics.rectangle('fill', x, y, unitWidth,unitHeight)

  love.graphics.rectangle('fill', (x - 1) * unitWidth, (y - 1) * unitHeight, unitWidth, unitHeight)
end

function drawlevel()
  for i = 1, maxXUnits do
    for j = 1, maxYUnits do
      if mapgrid[i][j] == 1 then
        love.graphics.setColor(1, 1, 200)
      elseif mapgrid[i][j] == 2 or mapgrid[i][j] == 3 then
        love.graphics.setColor(1, 200, 1)
      elseif mapgrid[i][j] == 4 then
        love.graphics.setColor(192, 192, 192)
      else
        love.graphics.setColor(1, 1, 1)
      end
      love.graphics.rectangle('fill', (i - 1) * unitWidth, (j - 1) * unitHeight, unitWidth, unitHeight)
    end
  end
end

function collisionWithMap()
  if x < 1 or y < 1 or x > maxXUnits or y > maxYUnits or mapgrid[x][y] == 1 then
    return true
  else
    return false
  end
end

function collisionWithNextRoom()
  if mapgrid[x][y] == NEXTROOMWARPERTILE then
    return true
  else
    return false
  end
end

function collisionWithPrevRoom()
  if mapgrid[x][y] == PREVROOMWARPERTILE then
    return true
  else
    return false
  end
end

function collisionWithCoinTile()
  if mapgrid[x][y] == COINTILE then
    return true
  else
    return false
  end
end

function currentlyOnWalkableArea()
  if mapgrid[x][y] == BLANKTILE or mapgrid[x][y] == COINTILE then
    return true
  else
    return false
  end
end

function buildDumbMap(val)
  print("build a map")
  -- build a dumb map
  grid = {}
  for i = 1, maxXUnits do
    grid[i] = {}

    for j = 1, maxYUnits do
      grid[i][j] = BLANKTILE -- Fill the values here
    end
  end

  for i = 1, maxXUnits do
    grid[1][i] = WALLTILE;
    grid[maxXUnits][i] = WALLTILE;
  end

  for i = 1, maxYUnits do
    grid[i][1] = WALLTILE;
    grid[i][maxYUnits] = WALLTILE;
  end

  grid[val][10] = WALLTILE;
  grid[val][11] = WALLTILE;
  grid[val][12] = WALLTILE;
  grid[val][13] = WALLTILE;
  grid[val][14] = WALLTILE;
  grid[val][15] = WALLTILE;
  grid[val][16] = WALLTILE;
  grid[val][17] = WALLTILE;
  grid[val][18] = WALLTILE;
  grid[val][19] = WALLTILE;

  return grid
end

function init_maze(width, height)
  grid = {}
  for i = 1, width do
    grid[i] = {}

    for j = 1, height do
      grid[i][j] = WALLTILE -- Fill the values here
    end
  end

  for i = 1, width do
    grid[i][1] = BLANKTILE;
    grid[i][height] = BLANKTILE;
  end

  for i = 1, height do
    grid[1][i] = BLANKTILE;
    grid[width][i] = BLANKTILE;
  end

  return grid
end

-- Carve the maze starting at x, y.
function carve_maze(maze, x, y, seed)
  maze[y][x] = COINTILE
  for i = 0, 3 do
    local d = (i + seed) % 4
    local dx = 0
    local dy = 0
    if d == 0 then
      dx = 1
    elseif d == 1 then
      dx = -1
    elseif d == 2 then
      dy = 1
    else
      dy = -1
    end
    local nx = x + dx
    local ny = y + dy
    local nx2 = nx + dx
    local ny2 = ny + dy
    if maze[ny][nx] == WALLTILE then
      if maze[ny2][nx2] == WALLTILE then
        maze[ny][nx] = COINTILE
        seed = seed + 11
        if seed > 100 then
          seed = 0
        end
        carve_maze(maze, nx2, ny2, seed)
      end
    end
  end
end

function print_maze(maze, width, height)
  for y = 1, height do
    for x = 1, width do
      io.write(maze[y][x])
    end
    io.write("\n")
  end
end

function prevYValue()
  return math.floor(maxYUnits / 2)
end

function nextYValue()
  return math.floor(maxYUnits / 2)
end

function addNextPrevMarkers(maze)
  maze[1][prevYValue()] = 2
  maze[maxXUnits][nextYValue()] = 3
end

function mirror_maze(maze)
  halfwidth = math.floor(maxXUnits / 2)

  for i = halfwidth, 0 do

  end

end

function buildLessDumbMap()
  -- The size of the maze (must be odd).

  if mazes[mapseed] ~= nil then
    print("Retrieving maze at seed " .. mapseed)

    maze = mazes[mapseed]
  else
    print("Generating maze with seed " .. mapseed)
    -- Generate and display a random maze.
    maze = init_maze(maxXUnits, maxYUnits)
    --print_maze(maze, maxUnits, maxUnits)
    carve_maze(maze, 2, 2, mapseed)
    addNextPrevMarkers(maze)
    saveMap()
  end
  return maze
end

function buildMirroredMap()
  -- The size of the maze (must be odd).

  if mazes[mapseed] ~= nil then
    print("Retrieving maze at seed " .. mapseed)

    maze = mazes[mapseed]
  else
    print("Generating maze with seed " .. mapseed)
    -- Generate and display a random maze.
    maze = init_maze(maxXUnits, maxYUnits)
    --print_maze(maze, maxUnits, maxUnits)
    carve_maze(maze, 2, 2, mapseed)
    -- throw the right half away by mirroring the left half
    mirror_maze(maze)
    addNextPrevMarkers(maze)
    saveMap()
  end
  return maze
end

function saveMap()
  mazes[mapseed] = maze
end

function love.keypressed(key, u)
  --Debug
  if key == "rctrl" then --set to whatever key you want to use
    debug.debug()
  end

  -- if key == "escape" then
  --   love.event.quit()
  -- end

  if key == " " then --set to whatever key you want to use
    print("space hit")
    saveMap()
    mapseed = mapseed + 1
    mapgrid = buildLessDumbMap(mapseed)
  end
end

function getAxisValue(val)
  ret = 0
  -- lamest deadzone ever
  if val and (val > 0.5 or val < - 0.5) then
    ret = val
  end

  return ret
end
