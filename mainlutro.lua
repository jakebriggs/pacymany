function lutro.load()
	x = 2
	y = 2
	onRoomWarper = false

	BLANKTILE = 0
	WALLTILE = 1
	PREVROOMWARPERTILE = 2
	NEXTROOMWARPERTILE = 3
	COINTILE = 4

	MAXSEED = 100
	MINSEED = 0
  print('here we go')
	--math.randomseed(os.time())
	--mapseed = math.random(0, 100)
	mapseed = 1
	-- maxUnits = 15
	maxXUnits = 15
	maxYUnits = 15

	elapsedTime = 0
	mazes = {}
	-- mapgrid = buildDumbMap(mapseed)
  mapgrid = buildLessDumbMap(mapseed)

  viewwidth = lutro.graphics.getWidth( )
	viewheight = lutro.graphics.getHeight( )

	unitWidth = viewwidth/maxXUnits;
	unitHeight = viewheight/maxYUnits;

	coinget = lutro.audio.newSource("coinget.wav", "static")

	print(viewwidth)
	print(viewheight)
	print(unitWidth)
	print(unitHeight)
end

function lutro.update(dt)
		--print(elapsedTime .. " - " .. dt)
    elapsedTime = elapsedTime + dt

	  if elapsedTime > 0.1 then
			elapsedTime = 0
			prevx = x
			prevy = y

			if lutro.keyboard.isDown("right") then
			    x = x + 1
			elseif lutro.keyboard.isDown("left") then
			    x = x - 1
			end

			if collisionWithMap() then
				x = prevx
			end

			if lutro.keyboard.isDown("down") then
	    	y = y + 1
			elseif lutro.keyboard.isDown("up") then
	    	y = y - 1
			end

			if collisionWithMap() then
				y = prevy
			end

			if collisionWithCoinTile() then
				coinget:stop()
				coinget:play()
				mapgrid[x][y] = BLANKTILE
			end

			if not onRoomWarper then
				if collisionWithNextRoom() then
					saveMap()
					mapseed = mapseed + 1
					if mapseed > MAXSEED then
						mapseed = MINSEED
					end
				 	mapgrid = buildLessDumbMap(mapseed)

					onRoomWarper = true

					x = 1
					y = prevYValue()
				elseif collisionWithPrevRoom() then
					saveMap()
					mapseed = mapseed - 1
					if mapseed < MINSEED then
						mapseed = MAXSEED
					end
					mapgrid = buildLessDumbMap(mapseed)

					onRoomWarper = true

					x = maxXUnits
					y = nextYValue()
				end
			end

			if currentlyOnWalkableArea() then
				onRoomWarper = false
			end

		end
end

function lutro.draw()
	drawlevel()
	drawactors()
end

function drawactors()
	lutro.graphics.setColor(224, 214, 68)
	-- lutro.graphics.rectangle('fill', x, y, unitWidth,unitHeight)

	lutro.graphics.rectangle('fill', (x - 1) * unitWidth, (y - 1) * unitHeight, unitWidth,unitHeight)
end

function drawlevel()
	for i = 1, maxXUnits do
			for j = 1, maxYUnits do
			  if mapgrid[i][j] == 1 then
					lutro.graphics.setColor(1, 1, 200)
				elseif mapgrid[i][j] == 2 or mapgrid[i][j] == 3 then
					lutro.graphics.setColor(1, 200, 1)
				elseif mapgrid[i][j] == 4 then
					lutro.graphics.setColor(192, 192, 192)
				else
					lutro.graphics.setColor(1, 1, 1)
				end
				lutro.graphics.rectangle('fill', (i - 1) * unitWidth, (j - 1) * unitHeight, unitWidth, unitHeight)
			end
	end
end

function collisionWithMap()
	if x < 1 or y < 1 or x > maxXUnits or y > maxYUnits or mapgrid[x][y] == 1 then
	  return true
	else
	  return false
	end
end

function collisionWithNextRoom()
	if mapgrid[x][y] == NEXTROOMWARPERTILE then
		return true
	else
		return false
	end
end

function collisionWithPrevRoom()
	if mapgrid[x][y] == PREVROOMWARPERTILE then
		return true
	else
		return false
	end
end

function collisionWithCoinTile()
	if mapgrid[x][y] == COINTILE then
		return true
	else
		return false
	end
end

function currentlyOnWalkableArea()
	if mapgrid[x][y] == BLANKTILE or mapgrid[x][y] == COINTILE then
		return true
	else
		return false
	end
end

function buildDumbMap(val)
	print("build a map")
	-- build a dumb map
	grid = {}
	for i = 1, maxXUnits do
	    grid[i] = {}

	    for j = 1, maxYUnits do
	        grid[i][j] = BLANKTILE -- Fill the values here
	    end
	end

	for i = 1, maxXUnits do
		grid[1][i]=WALLTILE;
		grid[maxXUnits][i]=WALLTILE;
	end

	for i = 1, maxYUnits do
		grid[i][1]=WALLTILE;
		grid[i][maxYUnits]=WALLTILE;
	end

	grid[val][10]=WALLTILE;
	grid[val][11]=WALLTILE;
	grid[val][12]=WALLTILE;
	grid[val][13]=WALLTILE;
	grid[val][14]=WALLTILE;
	grid[val][15]=WALLTILE;
	grid[val][16]=WALLTILE;
	grid[val][17]=WALLTILE;
	grid[val][18]=WALLTILE;
	grid[val][19]=WALLTILE;

	return grid
end

function init_maze(width, height)
	grid = {}
	for i = 1, width do
			grid[i] = {}

			for j = 1, height do
					grid[i][j] = WALLTILE -- Fill the values here
			end
	end

	for i = 1, width do
		grid[i][1]=BLANKTILE;
		grid[i][height]=BLANKTILE;
	end

	for i = 1, height do
		grid[1][i]=BLANKTILE;
		grid[width][i]=BLANKTILE;
	end

  return grid
end

-- Carve the maze starting at x, y.
function carve_maze(maze, x, y, seed)
   maze[y][x] = COINTILE
   for i = 0, 3 do
      local d = (i + seed) % 4
      local dx = 0
      local dy = 0
      if d == 0 then
         dx = 1
      elseif d == 1 then
         dx = -1
      elseif d == 2 then
         dy = 1
      else
         dy = -1
      end
      local nx = x + dx
      local ny = y + dy
      local nx2 = nx + dx
      local ny2 = ny + dy
      if maze[ny][nx] == WALLTILE then
         if maze[ny2][nx2] == WALLTILE then
            maze[ny][nx] = COINTILE
            seed = seed + 11
            if seed > 100 then
              seed = 0
            end
            carve_maze(maze, nx2, ny2, seed)
         end
      end
   end
end

function print_maze(maze, width, height)
  for y = 1, height do
     for x = 1, width do
        io.write(maze[y][x])
     end
     io.write("\n")
  end
end

function prevYValue()
	return math.floor(maxYUnits/2)
end

function nextYValue()
	return math.floor(maxYUnits/2)
end

function addNextPrevMarkers(maze)
	maze[1][prevYValue()] = 2
	maze[maxXUnits][nextYValue()] = 3
end

function mirror_maze(maze)
	halfwidth = math.floor(maxXUnits/2)

	for i = halfwidth, 0 do

	end

end

function buildLessDumbMap()
	-- The size of the maze (must be odd).

	if mazes[mapseed] ~= nil then
		print("Retrieving maze at seed " .. mapseed)

		maze = mazes[mapseed]
	else
		print("Generating maze with seed " .. mapseed)
		-- Generate and display a random maze.
		maze = init_maze(maxXUnits, maxYUnits)
	  --print_maze(maze, maxUnits, maxUnits)
		carve_maze(maze, 2, 2, mapseed)
		addNextPrevMarkers(maze)
		saveMap()
	end
	return maze
end

function buildMirroredMap()
	-- The size of the maze (must be odd).

	if mazes[mapseed] ~= nil then
		print("Retrieving maze at seed " .. mapseed)

		maze = mazes[mapseed]
	else
		print("Generating maze with seed " .. mapseed)
		-- Generate and display a random maze.
		maze = init_maze(maxXUnits, maxYUnits)
	  --print_maze(maze, maxUnits, maxUnits)
		carve_maze(maze, 2, 2, mapseed)
	  -- throw the right half away by mirroring the left half
		mirror_maze(maze)
		addNextPrevMarkers(maze)
		saveMap()
	end
	return maze
end

function saveMap()
	mazes[mapseed] = maze
end

function lutro.keypressed(key, u)
  --Debug
  if key == "rctrl" then --set to whatever key you want to use
	  debug.debug()
  end

	if key == "escape" then
		 lutro.event.quit()
	end

	if key == " " then --set to whatever key you want to use
		print("space hit")
		saveMap()
	  mapseed = mapseed + 1
	 	mapgrid = buildLessDumbMap(mapseed)
	end
end
